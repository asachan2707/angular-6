import { Ng6ProjPage } from './app.po';

describe('ng6-proj App', function() {
  let page: Ng6ProjPage;

  beforeEach(() => {
    page = new Ng6ProjPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
